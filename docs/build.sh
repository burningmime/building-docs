set -eo pipefail
scriptFile=$(readlink -f "${BASH_SOURCE[0]}")
scriptDir=$(cd -P "$(dirname "$scriptFile")" >/dev/null && pwd)
cd "$scriptDir"
doctoc --notitle README.md
pandoc --embed-resources --standalone -c "github-pandoc.css" --metadata title="Burning Building" -o "README.html" "README.md"
outFile="../../burning-building/building/Assets/Burning Mime Software/Building/README.html"
rm -rf "$outFile"
mv "README.html" "$outFile"
