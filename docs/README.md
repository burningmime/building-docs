<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Getting started](#getting-started)
  - [Step 1: Creating a material set](#step-1-creating-a-material-set)
    - [Material UVs and tiling](#material-uvs-and-tiling)
  - [Step 2: Creating a prop set](#step-2-creating-a-prop-set)
    - [Custom holes](#custom-holes)
  - [Step 3: Creating a building](#step-3-creating-a-building)
    - [Editor tools](#editor-tools)
      - [Rotating editor camera while a tool is selected](#rotating-editor-camera-while-a-tool-is-selected)
    - [Mini-tutorial](#mini-tutorial)
- [Advanced features](#advanced-features)
  - [Runtime wall cutout and room hiding](#runtime-wall-cutout-and-room-hiding)
  - [Baking light maps and navigation meshes](#baking-light-maps-and-navigation-meshes)
  - [Creating colliders](#creating-colliders)
- [The API: Letting players design their own buildings](#the-api-letting-players-design-their-own-buildings)
- [Additional Notes](#additional-notes)
  - [Questions and technical support](#questions-and-technical-support)
  - [Package Structure](#package-structure)
  - [Credits/Legal](#creditslegal)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Getting started

[Most of this info is available in a video tutorial -- click here!](https://vimeo.com/801932289). I reccomend turning on
closed captions / subtitles.

## Step 1: Creating a material set

Create a new BuildingMaterialSet asset in your project:

![](images/create-material-set.png)

The material set controls what materials can appear on the floors and walls.  There are no 
restrictions about what materials or shaders you can use; you can use any materials you
like. Drag and drop materials into the bottom of the list to add them:

![](images/material-editor-drag.png)

Also note...

1. The first material in the list is used for the inside of walls and doorways. In many games/archviz simulations,
   this material will be covered up, so it won't matter. However, UVs are not generated naturally for these "internal"
   segments. The best way to solve this is to use a material that doesn't need UVs -- often using triplanar projection.
2. The ceiling material is used for all ceilings.
3. If you change the order, it will affect all existing buildings that use the material set. Buildings only store an
   index into this list. This can cause problems, but it can also be used to your advantage to edit the materials for
   all the buildings across your application.

|                                   |                                   |
|-----------------------------------|-----------------------------------|
| ![](images/material-editor-2.png) | ![](images/material-editor-1.png) |

### Material UVs and tiling

UVs will be generated so that unit in object space will cover a full UV range (0 to 1). This gives you maximum
freedom to set up the tiling on a per-material basis by using the material inspector.

![](images/tiling-example-1.png)
![](images/tiling-example-2.png)

If in doubt, a wall height of 3 meters and tiling of 1/3rd looks reasonable. You may want to set the tiling to 1/4 or
1/6 to cut down on repetition, or if the texture covers a large area.

For natural textures, or textures without obvious lines in them (such as dirt, concrete, stone, or painted plaster), you
can also use [stochastic sampling](https://blog.unity.com/technology/procedural-stochastic-texturing-in-unity) to avoid
some repetition -- but that's beyond the scope of this project.

Note that there will be some slight UV stretching or scrunching on corners. This is intentional so that patterns will
align correctly and continue around corners. The technique is inspired by *The Sims* series which does this same UV
stretching/scrunching so walls don't appear to cut each other off. It's rarely noticeable, and prevents a common issue
that happens with other modular building systems.

## Step 2: Creating a prop set

Props are used for objects **embedded inside the walls, like windows or doors**. You could also use them for ventilation
shafts, half-walls or partially destroyed walls. Props are not good for objects that exist on just one side of the
wall (such as a painting or clock), since there is no direction to them. 

Create the `BuildingPropSet` object like above, and add in the prefabs you wish to use for doors or windows.
Prefabsshould be centered at (0, 0) in the X-Z plane and be aligned along the X axis.

![](images/prop-prefab.png)

The prop set also determines the height of the walls:

![](images/prop-set-wall-height.png)

### Custom holes

If you have a door, window, or other prop that doesn't fit into the default holes, you may set the hole type to
"custom". Then click on the prop in the list. You may adjust the "segment count" to set how many segments of wall 
the prop covers (this allows for wide windows, French doors, large archways, etc). Then click "Generate Holes".
Keep in mind...

* This is still a highly experimental/in-progress feature.
* Holes will be generated from the **convex hull** of the polygon formed by the intersection of the prop mesh and
  the XY plane. In most cases, this is fine, but certain props will not look correct. You may need to manually 
  adjust your meshes to cover the concave parts to avoid obvious artifacts.
* The generation algorithm uses the **current wall height and partial height** to determine the holes. If you
  change the wall height or partial height in the prop set, you should go through and re-generate each hole.

The idea with this feature is to be "good enough" to cover most windows, doors, archways, ventilation shafts, etc.
There will always be some odd cases that it does not work for, in which case you'll need to make the wall itself
part of the prop. If you have a *convex* mesh for which it does not work and think it should, go ahead and email
`support@burningmime.com` and I will try to help.

## Step 3: Creating a building

Create a new GameObject and add the Building component. Select a MaterialSet and PropSet to use. Then set the maximum
size. Generally, you want this slightly bigger than you need, but don't make it massive, since a greater max size will
have reduced performance.

Burning Building is designed to be oriented along the X-Z direction, so the "Y" is the number of floors. You can rotate
the object itself if you need it along X-Y axis, but walls always run paralell to X or Z in object-local space.

After that, click "generate," and start editing your building!

### Editor tools

|                                 |                     |                                                                                                                                                                        |
|---------------------------------|---------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ![](images/icons/move.png)      | **Transform**       | Use Unity's default transformation tools.                                                                                                                              |
| ![](images/icons/painttile.png) | **Paint Tile**      | Paint tiles to the selected material. Control-click and drag to remove tiles. Right click to change selected tile to the one you clicked on (eyedropper).              |
| ![](images/icons/floodtile.png) | **Flood Tile**      | Flood fill tiles. Lets you set the tiles for a whole room at once. Right click to change selected tile to the one you clicked on (eyedropper).                         |
| ![](images/icons/placewall.png) | **Add/Remove Wall** | Click and drag to add walls. Control-click and drag over an existing wall to remove it.                                                                                |
| ![](images/icons/square.png)    | **Square**          | Click and drag to create a square of tiles surrounded by walls. Control-click and drag to demolish in an area. The demolish is the most useful part, in my experience. |
| ![](images/icons/paintwall.png) | **Paint Wall**      | Click and drag along walls to paint them to the selected material. Right click to change selected material to the one you clicked on (eyedropper).                     |
| ![](images/icons/floodwall.png) | **Flood Wall**      | Click to flood all connected walls that share a material to a different material. Right click to change selected material to the one you clicked on (eyedropper).      |
| ![](images/icons/pullwall.png)  | **Pull Wall**       | Click and drag to push out or pull in a whole wall segment at once.                                                                                                    |
| ![](images/icons/wallprop.png)  | **Windows/Doors**   | Click on a wall to set the prop (window/door/off) to the selected one. Right click to change selected prop to the one you clicked on (eyedropper).                     |
| ![](images/icons/flag.png)      | **Tile Flags**      | Allows you to hide the ceiling (eg for courtyards or custom roofs). Allows you to hide floors but leave ceilings (eg for an indoor pool).                              |

#### Rotating editor camera while a tool is selected

Because the right mouse button is used by Burning Building's tools, to rotate the camera, hold down the ALT
key on your keyboard and use the left mouse button. Alternately, you can select the "Transform" tool which
will allow you to navigate normally in the edit window.

### Mini-tutorial

Start by clicking the square tool, then dragging out a large room.

![](images/tutorial-1.png)

Click the add walls tool. Drag out some new walls in the room to split it into several rooms.

![](images/tutorial-2.png)

From there, click the flood tiles tool, select a material, and click any tile to paint a room to that color.

![](images/tutorial-3.png)

Click the flood walls tool. Paint both the outside and inside of the walls to materials you like. Here I've made the
whole outside brick, but the insides are painted or paneled different colors.

![](images/tutorial-4.png)

Click the prop tool. Select a door or window prop. Click on a wall to insert it into that wall.

![](images/tutorial-5.png)

Finally, try out the pull walls tool. Click on the pull icon, and drag some walls around so that they are the size you
want. Notice that the floor materials on either side of the wall are preserved.

![](images/tutorial-6.png)

Now click "up". This moves you up a floor, and lets you create an upstairs area.

![](images/tutorial-7.png)

Click "Down". Notice the upper floor disappears to make it easier to edit downstairs. If you don't want that, you can
uncheck the "cut floors" which will let you see the whole building at once.

![](images/tutorial-8.png)

Finally try demolishing a room. Select the square tool and hold down the **Control** or **Command** key. This lets you
demolish a whole area at once, includig both tiles and walls. Note if you just want to demolish walls, use the control
key when you have the *Add Walls Tool* selected. And same for the add tiles tool -- holding control there will just
remove tiles.

![](images/tutorial-9.png)

# Advanced features

## Runtime wall cutout and room hiding

You can "cut out" walls to make it easier for an isometric or user-controlled camera to see into rooms. 
To do this, add the `Building_RuntimeCutout` component to your building. Set the mode to camera hiding. Set the camera
to use on the component. Then at runtime, walls will be cut out based on that camera's orientation, and choose
a mode.

Room hiding requires some scripting. The `Runtime Hiding` demo scene and scripts shows this. Refer to the comments in
those scripts for a full explanation. Basically, you want to set the `roomMask` property on the `Building_RuntimeCutout`
component to the set of rooms you want visible. You can use this to reveal rooms as the player explores the map (for 
example, a Fog of War effect), or hide irrelevant rooms when the player is not inside them.

## Baking light maps and navigation meshes

You may want to bake navigation or lighting for a building you create in the editor. However, by default the building is
not actually a real mesh that's saved anywhere -- it's generated at runtime. Further, if you're using wall cutting or
room hiding, the whole mesh can change from frame to frame. So it can't be static. However, you can create a "static 
proxy" in the editor. Then do whatever baking you want with this object. Once it's done, you remove the proxy, and 
bring back the regular building object.

1. Select the building object
2. Click "create static proxy" in the editor
3. Bake your light maps, nav mesh, etc.
4. Click "Destroy proxy and re-enable building"

## Creating colliders

If you want the building to have a MeshCollider select UNITY_MESH_COLLIDER from this dropdown:

![](images/collider-dropdown.png)

Note that you **should not add a MeshCollider component in the editor -- it will be automatically added at runtime**!

This will automatically generate a collider in play mode, based on the whole building mesh (it does not support
runtime hiding or cutouts, since typically you wouldn't want those things in a collider). Note that if your building
frequently changes, this can lead to frame drops/slowdown as Unity re-generates the collider internally. It's also
not the most efficient collider because it is convex and uses the fully detailed mesh -- if your game is static,
you may want to make your own, simplified physics colliders.

If all you need is raycasts to the currently visible mesh, you can use the INTERNAL_RAYCAST_ONLY mode.
This is good for making a click-to-move system like in many strategy or top-down RPG games, and it's already
generated because it's needed for the edit mode. It is not completely accurate, however (for example, you can't click
through doors; it will simply detect the hit on the wall instead of going through the hole).

# The API: Letting players design their own buildings

Most things you can do in the editor is also available at runtime via scripts. There are two demos showcasing this 
functionality -- the *Player Editor* scene extends all the tools from the Unity editor into the built
player. The *Random Maze* scene will generate a new, unique, maze every time it runs.

The workflow from the scripting side is...

1. Create or access an existing `Building` component.
2. Modify its `wallsX`, `wallsZ` and `tiles` properties.
3. Call `notifyLayoutChanged` on the Building object.
4. Visible changes to the mesh will have a 1-frame delay, because they are done in a separate thread.

# Additional Notes

## Questions and technical support

For technical support or questions, please either use the thread on the Unity Forums or email `support@burningmime.com`.
Also please report any bugs or issues you find -- Burning Building is a new package with a lot of complex code; so there's
bound to be problems.

## Package Structure

When you import the package, the files are split into two parts. Everything under the `Assets/Burning Mime Software`
folder is documentation or demos and can be removed from your project. The files required for it to run are under 
the `Packages/com.burningmime.building` folder.

## Credits/Legal

Burning Building copyright (C) Burning Mime Software, LLC 2023. It's available under the terms of the [Asset Store EULA](https://unity.com/legal/as-terms).

Licenses to Burning Building are sold on a [per-seat basis](https://support.unity.com/hc/en-us/articles/208601846-A-package-I-want-to-purchase-on-the-Asset-Store-says-Extension-Asset-One-license-required-for-each-individual-user-under-the-License-section-What-does-this-mean-).
If multiple people are developing a project using Burning Building, please buy a license for each team member who will
be using it.

Code under the `Packages/com.burningmime.building/src/external` folder is taken from (an old version of) the ECS and Unity Physics packages
by Unity Technologies, ApS. It's available under the terms of the [Unity Companion License](https://unity.com/legal/licenses/unity-companion-license).

Demos were created using assets from [AmbientCG](https://ambientcg.com/list?type=Atlas,Decal,Material), licensed under
the [Creative Commons CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/). You are free to use and redistribute them
as you see fit (no attribution is required).

Some of the icons contain content from the [Visual Studio Image Library](https://www.microsoft.com/en-us/download/details.aspx?id=35825).
The license is part of the download pack there. You are free to use and redistribute them as you see fit (no attribution is required).

Includes a compiled version of the [Clipper2 Library](https://github.com/AngusJohnson/Clipper2) by Angus Johnson. Clipper2 is released
under the terms of the [Boost Software License 1.0](https://www.boost.org/LICENSE_1_0.txt).

Includes a compiled version of the [Triangle.NET Library](https://github.com/wo80/Triangle.NET) by Christian Woltering. Triangle.NET is based
on the [Triangle](https://www.cs.cmu.edu/~quake/triangle.html) library by Richard Shewchuk. Triangle.NET was released under the terms of the
[MIT License](https://opensource.org/license/mit/).